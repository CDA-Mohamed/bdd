package fr.afpa.metiers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.afpa.beans.Table;
import fr.afpa.controle.Controle;

public class GestionTables {

	/**
	 * Méthode permettant à l'utilisateur de créer une table
	 * @param Elle prend en paramètre une table
	 */
	public void creerTable (String cmd) {
		String nomTable = cmd.split(" ")[2];
		String nomTable2 = nomTable.split("\\(")[0];
		if (Controle.checkTable(nomTable2)) {
			System.out.println("Désolé, la table que vous avez saisi existe déjà. Réessayer avec un autre nom de table");
		}
		else {
			// Créer un fichier nomTable.cda dans le répertoire C:/ENV/Projet_SGBD/listOfTables et ecrire les champs sur la première ligne
			
			File table = new File("C:\\ENV\\Projet_SGBD\\listOfTables\\"+nomTable2+".cda");
			FileWriter fw = null;
			BufferedWriter bw = null;
			String param = cmd.split("\\(")[1];
			String param2 = param.split("\\)")[0];
			String [] param3 = param2.split(",");
			try {
				fw = new FileWriter("C:\\ENV\\Projet_SGBD\\listOfTables\\"+nomTable2+".cda", true);
				bw = new BufferedWriter(fw);
				for(int i = 0; i < param3.length; i++) {
					bw.write(param3[i]+";");					
				}
				System.lineSeparator();
				System.out.println("La table "+nomTable2+ " a bien été crée !");
			} catch (Exception e) {
				System.out.println("Erreur d'écriture " + e);
			}
			finally {
				if (bw != null) 
					try {
						bw.close();
					}
			
			
				catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				}
			
			
			}
			
		}
	}
	
	/**
	 * Méthode permettant à l'utilisateur de insérer une ligne
	 * @param Elle prend en paramètre la commande de l'utilisateur
	 */
	public void insererLigne (String cmd) {

		String nomDeTable = cmd.split(" ")[2];
		
		if(!Controle.checkInsertSyntax(cmd)) {
			System.out.println("Veuillez respecter la syntaxe suivante :\n"
					+ "INSERT INTO nomTable VALUES('valeur1','valeur2'...); ");
		}
		
		else if(!Controle.checkTable(nomDeTable)){
			System.out.println("La table "+ nomDeTable + " est introuvable, veuillez réessayer." );
			
		}
		
		else if(!Controle.checkNbChamps(cmd)) {
			System.out.println("le nombre de paramètres ne correpond pas aux nombres de champs de la table "+nomDeTable+" .");
		}
		
		else {
			FileWriter fw = null;
			BufferedWriter bw = null;
			String param = cmd.split("\\(")[1];
			String param2 = param.split("\\)")[0];
			String [] param3 = param2.split(",");
			
			
			try {
				
				 fw = new FileWriter("C:\\ENV\\Projet_SGBD\\listOfTables\\"+nomDeTable+".cda" ,true);
				 bw = new BufferedWriter(fw);
				 for(int i = 0 ; i< param3.length ; i++) {
					bw.write(param3[i]+";");	
				 }
				 System.lineSeparator();
				 System.out.println("Votre insertion a bien été effectuée.");
				 
				 
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally {
				if (bw != null) 
					try {
						bw.close();
					}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	
	/**
	 * Méthode permettant à l'utilisateur d'afficher une table
	 * @param Elle prend en paramètre le nom de la table
	 */
	public void afficherTable (String nomTable) {
		
	}
	
	/**
	 * Méthode permettant à l'utilisateur d'afficher un  ou plusieurs champs d'une table donné 
	 * @param Elle prend en paramètre le nom de la table et la ligne de champs du fichier table (première ligne du fichier)
	 */
	public void afficherChamp (String nomTable, String ligneChamp) {
		
	}
	
	/**
	 * Méthode permettant à l'utilisateur de trier une table dans l'ordre croissant selon le champ voulu
	 * @param Elle prend en paramètre le nom de la table et le nom du champ qu'on veut trier
	 */
	public void trierChampsCroi (String nomTable , String nomChamp) {
		
	}
	
	/**
	 * Méthode permettant à l'utilisateur de trier une table dans l'ordre décroissant selon le champ voulu
	 * @param Elle prend en paramètre le nom de la table et le nom du champ qu'on veut trier
	 */
	public void trierChampsDec (String nomTable , String nomChamp) {
		
	}
	
	/**
	 * Méthode permettant à l'utilisateur de modifier le nom d'un champ d'une table donné 
	 * @param Elle prend en paramètre le nom de la table et le nom du champ
	 */
	public void modifierChamp (String cmd) {
		
	}
	
	/**
	 * Méthode permettant à l'utilisateur de modifier le nom de 2 champs d'une table donné 
	 * @param Elle prend en paramètre le nom de la table et le nom des 2 champs
	 */
	public void modifierChamps (String cmd) {
		String nomTable = cmd.split(" ")[1];
		if(!Controle.checkUpdateSyntax(cmd)) {
			System.out.println("Veuillez vérifier la syntaxe de votre commande. Pour modifier une valeur, tapez une des commandes suivantes :\n"
					+ "UPDATE nomTable SET nomChamp1='nouvelle_valeur',nomChamp2='nouvelle_valeur2'...;\n"
					+ "UPDATE nomTable SET nomChamp1='nouvelle_valeur';\n"
					+ "UPDATE nomTable SET nomChamp='nouvelle_valeur' WHERE nomChamp='ancienne_valeur';");
		}
		
		else if(!Controle.checkTable(nomTable)){
			System.out.println("La table "+ nomTable + " est introuvable, veuillez réessayer." );
			
		} 
		
		else {
			String nomChamp = cmd.split(" ")[3];
			String val = cmd.split("'")[1];
			FileReader fr = null;
			BufferedReader br = null;
			String values = "";
			try {
				fr = new FileReader("C:\\ENV\\Projet_SGBD\\listOfTables\\"+nomTable+".cda");
				br = new BufferedReader(fr);
				String champs = br.readLine();
				String [] ligneChamps = champs.split(";");
				int nbChamps = ligneChamps.length;
				int compteur = 0;
				
				for (int i = 0; i < ligneChamps.length ; i++) {
					if (!ligneChamps[i].equals(nomChamp)) {
						compteur++;
					}
					
					else {
						break;
					}
				}
				
				while (br != null) {
					
					values += br.readLine();
				}
				String [] allValues = values.split(";");
				for (int i = compteur; i < allValues.length; i += nbChamps ) {
					allValues[i] = val;
				}
				
				
			}catch(Exception e) {
				
			}
		}
		
	}
	
	/**
	 * Méthode permettant à l'utilisateur de modifier le nom d'un champ par un autre d'une table donné 
	 * @param Elle prend en paramètre le nom de la table et le nom du nouveau et de l'ancien champ
	 */
	public void modifierChampsWhere (String nomTable, String nouveauNom, String ancienNom) {
		
	}
	
	/**
	 * Cette méthode permet de vérifier si les données d'authentification du client existent dans le fichier d'authentification
	 * @param login du compteclient
	 * @param mot de passe du compte client
	 */
	public boolean authentification (String login, String mdp) {
	
		return true;
	}

	
	
}
