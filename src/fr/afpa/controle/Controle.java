package fr.afpa.controle;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import fr.afpa.beans.Table;

public class Controle {
	
	// CONTROLE DES DONNEES DE MANIERE TECHNIQUE
	
	/**
	 * Cette méthode permet de vérifier si la table existe dans la base de données
	 * @param Elle prend en paramètre une table
	 * @return Elle retrourne true si la table existe et false si elle n'existe pas
	 */
	public static boolean checkTable (String nomTable) {
		File rep = new File("C:\\ENV\\Projet_SGBD\\listOfTables");				//Repertoire de stockage des fichiers de table
		File [] listeFichiers = rep.listFiles();									// Creation d'un tableau de fichier à partir du dossier listOfTables
		String nomFichierExt = "";
		String nomFichier = "";
		for (int i = 0; i < listeFichiers.length; i++) {
			nomFichierExt = listeFichiers[i].getName();								// Récuperation des noms de fichiers avec extension et comparaison avec la valeur entrée par l'utilisateur
			nomFichier = nomFichierExt.substring(0, nomFichierExt.length()-4); 		// On compare le nom du fichier en enlevant l'extension .cda
			if (nomTable.equals(nomFichier)) {
				return true; 
			} 
		}
		return false;
	}
	
	
	/**
	 * Cette méthode vérifie si le nombre de paramètres entré par l'utilisateur pour inserer une donnée est bien égale au nombre de champs de la table
	 * @param Elle prend en paramètre le nombre de champs entrés par l'utilisateur ainsi qu'une table
	 * @return Elle retourne vrai si le nombre de paramètre est égal au nombre de champs de la table
	 */
	
public static boolean checkNbChamps (String cmd) {
		String [] commande = cmd.split(" ");
		
		String nomTable = cmd.split(" ")[2];				// Récuperation du nom de la table avec un split
		String [] nbParam = cmd.split(",");					// Recherche du nombre de paramètres entrés par l'utilisateur avec un split
		int nbChamps = nbParam.length;				
		FileReader fr = null;
		BufferedReader br = null;

		int nbChampsTable = 0;
		try {
			fr = new FileReader ("C:\\ENV\\Projet_SGBD\\listOfTables\\"+nomTable+".cda");  // Lecture du fichier table correspondant à la commande entrée
			br = new BufferedReader(fr);
			try {
				String [] champsTable = br.readLine().split(";");
				nbChampsTable = champsTable.length;			// Récupération du nombre de champs de la table via le fichier avec un split

			} catch (IOException e) {
				System.out.println("Désolé, un problème est survenue pendant la lecture du fichier, veuillez réessayer.");
			} 
		} catch (FileNotFoundException e) {
			
		}
		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		if (nbChamps == nbChampsTable) {										// Comparaison du nombre de champs avec le nombre de paramètres entrés par l'utilisateur
		return true;
	}
		return false;
}

// CONTROLE SYNTAXIQUE DES DONNEES


/**
 * Cette méthode controle la commande de création de table
 * @param Elle prend en paramètre la ligne de commande entré par l'utilisateur
 * @return Elle retourne vrai si la commande n'a pas d'erreur de syntaxe
 */
public static boolean checkCreateSyntax (String cmd) {
	try {
		Pattern p = Pattern.compile("(?i)(CREATE TABLE) [A-Za-z_\\d]{1,25}\\(([A-Za-z_\\d]{1,25},)*[A-Za-z_\\d]{1,25}\\);");
		Matcher m = p.matcher(cmd);
		if (m.matches()) {
			return true;
		}
	} catch (PatternSyntaxException pse) {
		
	}
	return false;
}

/**
 * Cette méthode controle la commande d'insertion de données
 * @param Elle prend en paramètre la ligne de commande entré par l'utilisateur
 * @return Elle retourne vrai si la commande n'a pas d'erreur de syntaxe
 */
public static boolean checkInsertSyntax (String cmd) {
	try {
		Pattern p = Pattern.compile("INSERT INTO [A-Za-z_\\d]{1,25} VALUES\\(('[A-Za-z_\\d]{1,25}',)*'[A-Za-z_\\d]{1,25}'\\);");
		Matcher m = p.matcher(cmd);
		if (m.matches()) {
			return true;
		}
	} catch (PatternSyntaxException pse) {
		
	}
	return false;
}

/**
 * Cette méthode controle la commande d'affichage de table
 * @param Elle prend en paramètre la ligne de commande entré par l'utilisateur
 * @return Elle retourne vrai si la commande n'a pas d'erreur de syntaxe
 */
public static boolean checkDisplaySyntax (String cmd) {
	try {
		Pattern p1 = Pattern.compile("SELECT \\* FROM [A-Za-z_\\d]{1,25}( Order by [A-Za-z_\\d]{1,25} (ASC|DESC))*;");   // Regex des 2 commandes SELECT FROM
		Matcher m1 = p1.matcher(cmd);
		
		Pattern p2 = Pattern.compile("SELECT [A-Za-z_\\d]{1,25}(,[A-Za-z_\\d]{1,25})* FROM [A-Za-z_\\d]{1,25};");		// Regex de la commande SELECT
		Matcher m2 = p2.matcher(cmd);
		if (m1.matches() || m2.matches()) {
			return true;
		}
	} catch (PatternSyntaxException pse) {
		
	}
	return false;
}

public static boolean checkUpdateSyntax (String cmd) {
	try {
		Pattern p1 = Pattern.compile("(?i)(UPDATE) [A-Za-z_\\d]{1,25} (?i)(SET) ([A-Za-z_\\d]{1,25}='[A-Za-z_\\d]{1,25}',)*[A-Za-z_\\d]{1,25}='[A-Za-z_\\d]{1,25}';");   // Regex des 2 commandes SELECT FROM
		Matcher m1 = p1.matcher(cmd);
		
		Pattern p2 = Pattern.compile("(?i)UPDATE [A-Za-z_\\d]{1,25} (?i)(SET) [A-Za-z_\\d]{1,25}='[A-Za-z_\\d]{1,25}' (?i)(WHERE) [A-Za-z_\\d]{1,25}='[A-Za-z_\\d]{1,25}';");		// Regex de la commande SELECT
		Matcher m2 = p2.matcher(cmd);
		if (m1.matches() || m2.matches()) {
			return true;
		}
	} catch (PatternSyntaxException pse) {
		
	}
	return false;
}


/**
 * Cette méthode controle l'authentification de l'utilisateur
 * @param Elle prend en paramètre 2 chaines de caracteres login et mdp
 * @return Elle retourne vrai si la commande n'a pas d'erreur de syntaxe
 */
public static boolean authSyntax (String login, String mdp) {
	
	return true;
}

}
