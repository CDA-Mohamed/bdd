package fr.afpa.beans;

public class Champ {
	private String libelle;
	final int LARGEUR = 30;
	
	public Champ (String libelle) {
		this.libelle = libelle;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public int getLARGEUR() {
		return LARGEUR;
	}

}
