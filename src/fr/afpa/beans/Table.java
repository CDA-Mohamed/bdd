package fr.afpa.beans;

import java.util.ArrayList;

public class Table {
	
	private String libelle;
	private ArrayList <String> champs;
	
	public Table (String libelle, Champ [] champs) {
		this.libelle = libelle;
		this.champs = new ArrayList <String> ();
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	

}
